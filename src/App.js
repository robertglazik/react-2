import Homepage from './pages/Homepage';
import './App.css';
import React from 'react';

function App() {

  return (
    <div className="App">
      <Homepage/>
    </div>
  );
}

export default App;
