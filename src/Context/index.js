import React, {createContext, useState} from 'react';

export const CharStats = createContext({
    name: "",
    str: 0,
    hp: 0,
    speed: 0,
    addStr: item =>{}
})





export const UserStatsProvider = ({ children }) =>{
    const name = "Goblin";
    const [str, addStr] = useState(10);
    const hp = 12;
    const speed = 2;

    return(
        <CharStats.Provider
            value={{
            name,
            str,
            hp,
            speed,
            addStr
        }}
        >
            {children}
        </CharStats.Provider>
    )
}

export const OponentStatsProvider = ({ children}) =>{
    const name = "Demon";
    const str = 10;
    const hp = 12;

    return(
        <CharStats.Provider
            value={{
            name,
            str,
            hp
        }}
        >
            {children}
        </CharStats.Provider>
    )
}