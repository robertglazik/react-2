import React, {Component} from 'react';

export default class Child extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "Yeah, it's working!"
        };
    }
    render() {
        return (
            <div>
                <p>{this.state.name}</p>
            </div>
        );
    }
}