import { useContext } from 'react';
import {CharStats} from '../../Context';
import '../User/style.css';

const Oponent = (props) => {
    const {name, str, hp, speed} = useContext(CharStats);

    return (
        <div className="col-6">
            <h1>Level: {props.level}</h1>
            <p>Name: {name}</p>
            <p>Strong: {str}</p>
            <p>Life: {hp}</p>    
            <p>Speed: {speed}</p>        
        </div>
    )
}
export default Oponent;