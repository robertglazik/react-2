import React from 'react';
import './style.css';

const User = (props) => {
    
    return (
        <div className="col-6">
            <h1>Level: {props.level}</h1>
            <p>Name: {props.name}</p>
            <p>Strong: {props.str}</p>
            <p>Life: {props.life}</p>
            <p>Speed: {props.speed}</p>                  
        </div>     
    )
}
export default User;