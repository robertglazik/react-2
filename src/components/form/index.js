import React  from 'react';
import User from './../User';
import {OponentStatsProvider} from '../../Context';
import Oponent from '../../components/Oponent';

class Form extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            showForm : true,
            name: '',
            strong: 1,
            life: (Math.floor(Math.random() * (20 - 1)) + 1),
            speed: (Math.floor(Math.random() * (20 - 1)) + 1),
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        let nam = e.target.name;
        let val = e.target.value;
        this.setState({[nam]: val});        
    }

    handleSubmit(e){
        e.preventDefault();        
    }

	render(){        
		return(
            <div>
                {
                    (this.state.showForm)&&(this.state.name!=undefined)?
                    <div>
                        <form onSubmit={this.handleSubmit}>
                            <label>Name: </label>
                            <input placeholder="Your name..." type="text" id="name" name="name" value={this.state.name} onChange={this.handleChange}/>
                            <label>Strong: </label>
                            <input type="number" name="strong" min="1" max="20" value={this.state.strong} onChange={this.handleChange}/>
                            <label>Life: </label>
                            <input type="number" disabled name="life" value={this.state.life} onChange={this.handleChange}/>
                            <label>Speed: </label>
                            <input type="number" disabled name="speed" value={this.state.speed} onChange={this.handleChange}/>
                            <input type="submit" name="send" onClick={()=>{this.setState({showForm: false})} }/>
                        </form>
                        </div>
                    :
                        <div className="Container">
                            <div  className="col-6">
                                <User 
                                level='10'
                                name={this.state.name} 
                                str={this.state.strong}
                                life={this.state.life}
                                speed={this.state.speed}
                                />
                            </div>
                                <div className="col-6">
                                <OponentStatsProvider>
                                    <Oponent level= "10" />
                                </OponentStatsProvider>
                            </div>
                        </div>
                }
            </div>
		)
	}
}



export default Form;